import tensorflow as tf 
import numpy as np

def custom_predict_fn(examples, serving_bundle):

    number_of_examples = len(examples)
    #model = tf.keras.models.load_model('/Users/florence/Desktop/gcp/AI_Platform/rossmann/local-training-output/keras_export/1', compile=False)
    model = tf.keras.models.load_model('gs://rossmann-ex/rossmann-job-dir/keras_export/1', compile=False)
    results = []
    for sample in examples:
        scores = []
        sample_features = sample.features.feature
        d = {}
        for elem in sample_features:
            d["{}".format(elem)] = sample_features[elem].float_list.value[0]            

        # delete the target value: 
        del d['target']

        feature_vector = np.fromiter(d.values(), dtype=float)
        feature_vector = feature_vector.reshape((-1, 20))
        result = model.predict(feature_vector)
        results.append(result)  # regression
    flat_results = [item for sublist in results for item in sublist]
    return results
