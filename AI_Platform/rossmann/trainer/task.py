"""This code implements a Feed forward neural network using Keras API for the Rossmann Data.
Model-Code comes from here: https://github.com/gregaw/sales-forecasting-with-nn """

import numpy as np
import argparse
import os 
from . import model
import tensorflow as tf 

def run(train_files, learning_rate, num_epochs, job_dir):

    # setting seed
    np.random.seed(13)

    keras_model = model.model_fn(learning_rate)
    train_x, train_y, eval_x, eval_y = model.transform_data(train_files)

    tensorboard_cb = tf.keras.callbacks.TensorBoard(
    os.path.join(job_dir, 'keras_tensorboard'),
    histogram_freq=1)

    keras_model.fit(train_x, train_y, epochs=num_epochs, batch_size=256, callbacks=[tensorboard_cb])
    score = keras_model.evaluate(eval_x, eval_y, verbose=1)
    print("Test score:", score[0])
    print("Test mae:", score[1])

    # model = save_model()
    export_path = os.path.join(args.job_dir, 'keras_export/1/') #this one is needed for the version number for TF Serving
    keras_model.save(export_path)


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--lr',
                        type=float,
                        default=0.003,
                        help='Learning rate for SGD')
    parser.add_argument('--epochs',
                        type=int,
                        default=20,
                        help='Maximum number of epochs on which to train')
    parser.add_argument('--train-files',
                        required=True,
                        type=str,
                        help='Training files local or GCS')
    parser.add_argument('--job-dir',
                        required=True,
                        type=str,
                        help='GCS or local dir to write checkpoints and export model')

    return parser

if __name__ == "__main__":
    parser = parse_args()
    args = parser.parse_args()
    print(args.lr, args.epochs)
    run(args.train_files, args.lr, args.epochs, args.job_dir)