"""Sales Forecast as a Neural Network model."""

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation

from sklearn.preprocessing import MinMaxScaler

LOOK_BACK = 20

FEATURES_CONT = []
FEATURES_WINDOW = ['Sales']
FEATURES_ALL = FEATURES_WINDOW + FEATURES_CONT

FEATURE_SIZE = LOOK_BACK + len(FEATURES_CONT)


def model_fn(learning_rate):
    model = Sequential()
    model.add(Dense(4, input_shape=(FEATURE_SIZE, )))
    model.add(Dense(1))

    optim = tf.keras.optimizers.Adam(learning_rate = learning_rate)
    model.compile(loss='mean_squared_error', optimizer=optim, metrics=['mae'])

    return model

def create_tf_example(features, label):
    tf_example = tf.train.Example(features=tf.train.Features(feature={
        't0':tf.train.Feature(float_list=tf.train.FloatList(value=[features[0]])),
        't1':tf.train.Feature(float_list=tf.train.FloatList(value=[features[1]])),
        't2':tf.train.Feature(float_list=tf.train.FloatList(value=[features[2]])),
        't3':tf.train.Feature(float_list=tf.train.FloatList(value=[features[3]])),
        't4':tf.train.Feature(float_list=tf.train.FloatList(value=[features[4]])),
        't5':tf.train.Feature(float_list=tf.train.FloatList(value=[features[5]])),
        't6':tf.train.Feature(float_list=tf.train.FloatList(value=[features[6]])),
        't7':tf.train.Feature(float_list=tf.train.FloatList(value=[features[7]])),
        't8':tf.train.Feature(float_list=tf.train.FloatList(value=[features[8]])),
        't9':tf.train.Feature(float_list=tf.train.FloatList(value=[features[9]])),
        't10':tf.train.Feature(float_list=tf.train.FloatList(value=[features[10]])),
        't11':tf.train.Feature(float_list=tf.train.FloatList(value=[features[11]])),
        't12':tf.train.Feature(float_list=tf.train.FloatList(value=[features[12]])),
        't13':tf.train.Feature(float_list=tf.train.FloatList(value=[features[13]])),
        't14':tf.train.Feature(float_list=tf.train.FloatList(value=[features[14]])),
        't15':tf.train.Feature(float_list=tf.train.FloatList(value=[features[15]])),
        't16':tf.train.Feature(float_list=tf.train.FloatList(value=[features[16]])),
        't17':tf.train.Feature(float_list=tf.train.FloatList(value=[features[17]])),
        't18':tf.train.Feature(float_list=tf.train.FloatList(value=[features[18]])),
        't19':tf.train.Feature(float_list=tf.train.FloatList(value=[features[19]])),
        'target':tf.train.Feature(float_list=tf.train.FloatList(value=[label])),

    }))
    return tf_example


def create_windows(dataset, look_back=1):
    """
    create windows of data
    :param dataset: first column (sales) is to be have row look back, rest is just appended
    :param look_back:
    :return: ( x:(n-LB-1, LB+1), y:(n-LB-1, 1) )
    """
    x, y = [], []
    for i in range(len(dataset) - look_back):
        row = dataset[i:(i + look_back), 0]
        row = np.append(row, dataset[i + look_back, 1:])  # appending CONT
        x.append(row)
        y.append(dataset[i + look_back, 0])
    return np.array(x), np.array(y)


def transform_data(train_file):
    df = pd.read_csv(train_file)

    msk = np.random.rand(len(df)) < 0.8
    train_df = df[msk]
    test_df = df[~msk]

    

    train_values = train_df[FEATURES_ALL].values
    train_values = train_values.astype('float32')

    eval_values = test_df[FEATURES_ALL].values
    eval_values = eval_values.astype('float32')

    # only taking Sales as features 
    train_x, train_y = create_windows(train_values, FEATURE_SIZE)
    eval_x, eval_y = create_windows(eval_values, FEATURE_SIZE)

    # this code is only needed to create tfrecord file for WIT 
    
    # print(eval_x)
    # print(eval_y)

    # eval_rows = np.concatenate((eval_x, eval_y[:, None]), axis=1)

    # print(eval_rows)
    # # save tfrecord file from eval_values
    # csv = eval_rows
    # with tf.io.TFRecordWriter("rossmann.tfrecord") as writer:
    #     for row in csv:
    #         features, label = row[:-1], row[-1]
    #         example = create_tf_example(features, label)
    #         writer.write(example.SerializeToString())
    # writer.close()

    return train_x, train_y, eval_x, eval_y

